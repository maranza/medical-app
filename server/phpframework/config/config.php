<?php
$config = [];
$config['database']['pgsql'] = [

    'host' => 'localhost',
    'username' => 'postgres',
    'password' => 'root',
    'port' => 5432,
    'database' => 'medical'
    
];
